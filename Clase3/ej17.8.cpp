// ej17.8.cpp

#include <iostream>

class CuentaAhorros
{
private:
	double tasaInteresAnual{ 0.0 };
	double saldoAhorro{ 0.0 };

public:
	CuentaAhorros() : tasaInteresAnual{ 0.0 }, saldoAhorro{ 0.0 } 
	{
		// nada
	}

	CuentaAhorros( double _nuevoSaldo ) :
		tasaInteresAnual{ 0.0 }, saldoAhorro{ _nuevoSaldo }
	{
		// nada
	}

	CuentaAhorros( double _nuevoSaldo, double _tasa ) :
		tasaInteresAnual{ _tasa },
		saldoAhorro{ _nuevoSaldo }
	{
		// nada
	}



	void ultimoInteresMensual()
	{
		saldoAhorro = saldoAhorro + (saldoAhorro * tasaInteresAnual ) / 12.0;
	}

	void modificaTasaInteres( double _tasa )
	{
		this->tasaInteresAnual = _tasa;
	}

	void ponerSaldo( double _nuevoSaldo )
	{
		this->saldoAhorro = _nuevoSaldo;
	}

	double ObtenerSaldo()
	{
		return this->saldoAhorro;
	}

};

int main(void)
{
	CuentaAhorros ahorrador1{ 2000.0 };
	CuentaAhorros ahorrador2{ 3000.0, 3.0 / 100.0 };

	std::cout << "Antes...\n";
	std::cout << "Saldo ahorrador 1: " << ahorrador1.ObtenerSaldo() << std::endl;
	std::cout << "Saldo ahorrador 2: " << ahorrador2.ObtenerSaldo() << std::endl;

	ahorrador1.modificaTasaInteres( 3.0 / 100.0 );

	ahorrador1.ultimoInteresMensual();
	ahorrador2.ultimoInteresMensual();

	std::cout << "\nDespués...\n";
	std::cout << "Saldo ahorrador 1: " << ahorrador1.ObtenerSaldo() << std::endl;
	std::cout << "Saldo ahorrador 2: " << ahorrador2.ObtenerSaldo() << std::endl;

}
