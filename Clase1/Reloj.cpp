// Reloj.cpp

#include <iostream>
// para la entrada/salida

class Reloj
{
public:
	void Poner( int _horas, int _minutos, int _segundos )
	{
		this->horas = _horas;
		this->minutos = _minutos;
		this->segundos = _segundos;
	}

	int GetHoras()
	{
		return this->horas;
	}

	int GetMinutos()
	{
		return this->minutos;
	}

	int GetSegundos()
	{
		return this->segundos;
	}

	void Imprimir()
	{
		std::cout << this->horas << ":" << this->minutos << ":" << this->segundos << std::endl;
	}

	void Tick()
	{
		++this->segundos;
		if( this->segundos > 59 ){
			this->segundos = 0;
			++this->minutos;
			if( this->minutos > 59 ){
				this->minutos = 0;
				++this->horas;
				if( this->horas > 23 ){
					this->horas = 0;
				}
			}
		}
	}

private:
	int horas;
	int minutos;
	int segundos;
};

int main()
{
	Reloj casio;
	casio.Poner( 13, 30, 45 );
	casio.Imprimir();

	std::cout << "Otra forma:\n";
	std::cout << casio.GetHoras() << ":";
	std::cout << casio.GetMinutos() << ":";
	std::cout << casio.GetSegundos() << std::endl;

	// simulamos que transcurrieron 60 segundos
	for( int i = 0; i < 60; ++i ){
		casio.Tick();
	}
	casio.Imprimir();
}
