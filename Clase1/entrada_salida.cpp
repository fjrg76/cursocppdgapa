// entrada_salida.cpp

#include <iostream>
// para la entrada/salida


int main()
{
	std::cout << "Introduzca un número entero: " << std::endl;

	int valor{ 0 };
	std::cin >> valor;

	int cuadrado = valor * valor;
	std::cout << "El cuadrado de "<< valor << " es " << cuadrado << std::endl;
}
