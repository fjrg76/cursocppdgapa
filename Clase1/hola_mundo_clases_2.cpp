// hola_mundo_clases_2.cpp

#include <iostream>
// para la entrada/salida

#include <string>
// para manejo de las cadenas

class HolaMundo
{
public:
	void Imprimir()
	{
		std::cout << msg << std::endl;
	}

	void SetMensaje( std::string _nuevoMensaje )
	{
		msg = _nuevoMensaje;
	}

private:
	std::string msg;
};

int main()
{
	HolaMundo h;

	h.SetMensaje( "Hello world" );
	h.Imprimir();
}
