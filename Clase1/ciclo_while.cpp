// ciclo_while.cpp

#include <iostream>
// para la entrada/salida


int main()
{
	bool continuar{ true };

	while( continuar == true ){
		std::cout << "Introduzca un número entero: " << std::endl;

		int valor{ 0 };
		std::cin >> valor;

		int cuadrado = valor * valor;
		std::cout << "El cuadrado de "<< valor << " es " << cuadrado << std::endl;


		std::cout << "Para salir escriba 0, o cualquier otro valor para continuar\n";
		int salir{ 0 };
		std::cin >> salir;

		if( salir == 0 ){
			continuar = false;
		}
		else{
			continuar = true;
		}
	}
}
