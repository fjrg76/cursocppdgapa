// ciclo_for.cpp

#include <iostream>
// para la entrada/salida

#include <cmath>
// para las funciones matemáticas

int main()
{
	int veces{ 0 };

	std::cout << "¿Cuántas veces desea repetir?" << std::endl;
	std::cin >> veces;
	if( veces < 1 ){
		std::cout << "ERROR: El número de repeticiones debe ser igual o mayor a 1\n";
	}
	else{
		for( int i = 0; i < veces; ++i ){
			std::cout << "-- " << i+1 << " --\n";

			std::cout << "Introduzca un número real: " << std::endl;
			double valor{ 0.0 };
			std::cin >> valor;

			double cubo = pow( valor, 3.0 );
			std::cout << "El cubo de "<< valor << " es " << cubo << std::endl;
		}
	}
}
