/*
 * excep3.cpp
 *
 * lanza una excepción estándar
 */

#include <iostream>
#include <string>
#include <stdexcept>


class Prueba
{
public:
	void UnMetodo( int val )
	{
		if( val > 0 ){
			std::cout << "Todo bien! " << val << std::endl;
		}
		else{
			throw std::range_error {"Hubo un error en el rango"};
		}
	}
};



int main(void)
{
	Prueba p;

	try
	{
		int res{0};
		std::cout << "Ingrese un valor mayor que 0: ";
		std::cin >> res;
		p.UnMetodo( res );
	}
	catch (std::range_error & e)
	{
		std::cout << e.what () << std::endl;
	}

}
