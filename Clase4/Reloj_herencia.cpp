
// Reloj_herencia.cpp


//----------------------------------------------------------------------
//  Clase Reloj
//----------------------------------------------------------------------
class Reloj
{
protected:
	int hrs;
	int min;

public:
	Reloj( int _hrs = 12, int _min = 0 ) :
		hrs{ _hrs }, min{ _min }
	{
		if( hrs < 0 or  hrs > 23 ) throw std::range_error{ "Error en el rango de las horas" }; 
		if( min < 0 or min > 59 ) throw std::range_error{ "Error en el rango de las minutos" }; 
	}

	void PonerHoras( int _hrs )
	{
		if( _hrs < 0 or _hrs > 23 ) throw std::range_error{ "Error en el rango de las horas" }; 
		hrs = _hrs;
	}

	void PonerMinutos( int _min )
	{
		if( min < 0 or min > 59 ) throw std::range_error{ "Error en el rango de las minutos" }; 
		min = _min;
	}

	int ObtenerHoras()
	{
		return hrs;
	}

	int ObtenerMinutos()
	{
		return min;
	}

	void Imprimir()
	{
		std::cout << hrs << ":" << min << std::endl;
	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{
		++min;
		if( min > 59 ){
			min = 0;

			++hrs;
			if( hrs > 23 ){
				hrs = 0;
			}
		}
	}
};


//----------------------------------------------------------------------
//  Clase RelojAMPM
//----------------------------------------------------------------------
class RelojAMPM : public Reloj
{
private:
	bool modo{ false };
	// false: am ... true: pm
	
public:
	RelojAMPM( int _hrs = 12, int _min = 0, bool _modo = false ) :
		Reloj{ _hrs, _min },
		modo{ _modo }
	{
		// escriba una excepción para las horas. Recuerde que éste es un reloj
		// de 12 hrs, por lo que ninguna hora debería estar fuera del rango 
		// [1, 12]
	}

	void PonerHoras( int _hrs )
	{
		// escriba una excepción para las horas. Recuerde que éste es un reloj
		// de 12 hrs, por lo que ninguna hora debería estar fuera del rango 
		// [1, 12]
	}


	void PonerModo( bool _modo )
	{
		// haga que la variable miembro 'modo' valga '_modo'
	}

	bool ObtenerModo()
	{
		// devuelva el valor de la variable miembro 'modo'
	}

	void Imprimir()
	{
		// recuerde que este reloj es de 12 hrs, por lo tanto necesita agregar
		// la cadena "AM" o "PM" a la impresión de la hora dependiendo del valor
		// de la variable miembro 'modo'
	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{
		// similar al ejercicio que hicimos; sin embargo, tome en cuenta que
		// éste es un reloj de 12 hrs. Por lo tanto, 11:59 PM + 1 pasa a ser
		// 12:00 PM
	}
};

int main()
{
	// Cree un nuevo reloj a partir de la clase RelojAMPM utilizando al
	// constructor con argumentos. Nómbrelo miReloj y use la siguiente hora:
	// 11:27 PM
	
	// pida al usuario la hora, los minutos e insértelos // en el objeto miReloj.
	// No importa que el reloj ya traiga una hora. 
	
	// imprima la hora actual
	
	// simulamos que transcurren 45 minutos (cuando llegue a este punto 
	// descomente la línea de código que está dentro del for)
	for( int i = 0; i < 45; ++i ){
		// ++miReloj;
	}

	// imprima la nueva hora ¿ésta nueva hora es la correcta cuando
	// transcurrieron 45 minutos?
	
	// imprima la hora utilizando las funciones miembro ObtenerHoras(),
	// ObtenerMinutos() y ObtenerModo()
	
	// Tip: Para imprimir si es am o pm puede usar el siguiente código
	// if( miReloj.ObtenerModo() == true ){
	//		std::cout << "PM.";
	// }
	// else{
	//		std::cout << "AM.";
	// }
	// std::cout << std::endl;
}
