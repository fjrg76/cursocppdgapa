
// herencia1.cpp

#include  <iostream>
#include <cmath>


class Figura2D
{
protected:
	double lado1;

public:
	double Area();
	double Perimetro();
	void Dibujar()
	{
		std::cout << "Dibujando una figura abstracta!\n";
	}

	Figura2D( double _lado = 0.0 ) : lado1{ _lado }
	{
		// nada
	}
};

class Circulo : public Figura2D
{
private:
	const double PI = 3.1415926;

public:
	Circulo( double _radio = 0.0 ) :
		Figura2D{ _radio }
	{
		// nada
	}

	double Perimetro()
	{
		return lado1 * 2.0 * PI;
	}

	double Area()
	{
		return PI * pow( lado1, 2.0 );
	}

	void Dibujar()
	{
		std::cout << "¡Soy un círculo de radio " << lado1 << "!\n";
	}

	void PonerRadio( double _nuevoRadio )
	{
		lado1 = _nuevoRadio;
	}
};

class Triangulo : public Figura2D
{
private:
	double lado2;
	double lado3;

public:
	Triangulo( double _lado1, double _lado2, double _lado3 ) :
		Figura2D{ _lado1},
		lado2{ _lado2}, lado3{ _lado3 }
	{
		// nada
	}
};

class Rectangulo : public Figura2D
{
private:
	double lado2;

public:
	Rectangulo( double _lado1, double _lado2 ) :
		Figura2D{ _lado1},
		lado2{ _lado2}
	{
		// nada
	}


};


int main(void)
{
	Circulo unCirculo{ 1.0 };
	unCirculo.Dibujar();
	unCirculo.PonerRadio( 2.0 );
	unCirculo.Dibujar();
	std::cout << "El área es: " << unCirculo.Area() << std::endl;
}
