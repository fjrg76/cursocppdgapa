
// Reloj_herencia_enum.cpp
//
// Esta versión utiliza una enumeración tipo C++11 (enum class) para el modo (am
// o pm) de la clase RelojAMPM


class Reloj
{
protected:
	int hrs;
	int min;

public:
	Reloj( int _hrs = 12, int _min = 0 ) :
		hrs{ _hrs }, min{ _min }
	{
		// aquí van las excepciones
	}

	void PonerHoras( int _hrs )
	{

	}

	void PonerMinutos( int _min )
	{

	}

	int ObtenerHoras()
	{

	}

	int ObtenerMinutos()
	{

	}

	void Imprimir()
	{

	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{

	}
};

class RelojAMPM : public Reloj
{
public:
	enum class Modo{ AM, PM };

	RelojAMPM( int _hrs = 12, int _min = 0, Modo _modo = Modo::AM ) :
		Reloj{ _hrs, _min },
		modo{ _modo }
	{

	}

	void PonerModo( Modo _modo )
	{

	}

	bool ObtenerModo()
	{

	}

	void Imprimir()
	{

	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{

	}

private:
	Modo modo{ Modo::AM };
	
};

int main()
{
	Reloj reloj;
	RelojAMPM relojampm{ 8, 46, RelojAMPM::Modo::AM };
}
