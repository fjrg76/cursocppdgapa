
// Reloj_herencia.cpp

#include <iostream>

#include <stdexcept>
// para usar las excepciones estándar


class Reloj
{
protected:
	int hrs;
	int min;

public:
	Reloj( int _hrs = 12, int _min = 0 ) :
		hrs{ _hrs }, min{ _min }
	{
		if( hrs < 0 ) throw std::range_error{ "Las horas no pueden ser negativas\n" }; 

		if( hrs > 23 ) throw std::range_error{ "Las horas no pueden ser mayores a 23\n" }; 

		if( min < 0 ) throw std::range_error{ "Los minutos no pueden ser negativos\n" }; 

		if( min > 59 ) throw std::range_error{ "Los minutos no pueden ser mayores a 59\n" }; 
	}

	void PonerHoras( int _hrs )
	{
		if( _hrs < 0 or _hrs > 23 ) throw std::range_error{ "Error en el rango de las horas" }; 
		hrs = _hrs;
	}

	void PonerMinutos( int _min )
	{
		if( _min < 0 ) 
			throw std::range_error{ "Error en el rango de las minutos" }; 
		
		if( _min > 59 ) 
			throw std::range_error{ "Error en el rango de las minutos" }; 
		
		min = _min;
	}

	int ObtenerHoras()
	{
		return hrs;
	}

	int ObtenerMinutos()
	{
		return min;
	}

	void Imprimir()
	{
		std::cout << hrs << ":" << min << std::endl;
	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{
		++min;
		if( min > 59 ){
			min = 0;

			++hrs;
			if( hrs > 23 ){
				hrs = 0;
			}
		}
	}
};

class RelojAMPM : public Reloj
{
private:
	bool modo{ false };
	// false: am ... true: pm
	
public:
	RelojAMPM( int _hrs = 12, int _min = 0, bool _modo = false ) :
		Reloj{ _hrs, _min },
		modo{ _modo }
	{

	}

	void PonerModo( bool _modo )
	{

	}

	bool ObtenerModo()
	{

	}

	void Imprimir()
	{

	}

	// incrementa los minutos en una unidad
	void operator++ ()
	{

	}
};

int main()
{
	int horas;
	int minutos;
	bool continuar{ false };
	Reloj r;
	

	while( continuar == false ){
		std::cout << "Ingrese las horas y los minutos: ";
		std::cin >> horas;
		std::cin >> minutos;

		try{
			r.PonerHoras( horas );
			r.PonerMinutos( minutos );
			
			continuar = true;
		}
		catch( std::range_error& e ){
			std::cout << e.what() << std::endl;
		}
	}

	r.Imprimir();
}
