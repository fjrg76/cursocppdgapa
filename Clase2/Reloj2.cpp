// Reloj2.cpp

#include <iostream>
// para la entrada/salida

class Reloj
{
public:
	Reloj();

	Reloj(int _horas, int _minutos, int _segundos );

	void Poner( int _horas, int _minutos, int _segundos );

	int GetHoras();

	int GetMinutos();

	int GetSegundos();

	void Imprimir();

	void Tick();

private:
	int horas;
	int minutos;
	int segundos;
};

Reloj::Reloj() :
	horas{ 12 }, minutos{ 0 }, segundos{ 0 }
{

}

Reloj::Reloj(int _horas, int _minutos = 0, int _segundos = 0 ) :
	horas{ _horas }, minutos{ _minutos }, segundos{ _segundos }
{

}

int Reloj::GetHoras()
{
	return this->horas;
}

int Reloj::GetMinutos()
{
	return this->minutos;
}

int Reloj::GetSegundos()
{
	return this->segundos;
}

void Reloj::Tick()
{
	++this->segundos;
	if( this->segundos > 59 ){
		this->segundos = 0;
		++this->minutos;
		if( this->minutos > 59 ){
			this->minutos = 0;
			++this->horas;
			if( this->horas > 23 ){
				this->horas = 0;
			}
		}
	}
}


int main()
{
	Reloj citizen{ 13, 30, 45 };
	citizen.Imprimir();


	Reloj casio;
	casio.Imprimir();

	std::cout << "Otra forma:\n";
	std::cout << casio.GetHoras() << ":";
	std::cout << casio.GetMinutos() << ":";
	std::cout << casio.GetSegundos() << std::endl;

	// simulamos que transcurrieron 60 segundos
	for( int i = 0; i < 60; ++i ){
		casio.Tick();
	}
	casio.Imprimir();
}
