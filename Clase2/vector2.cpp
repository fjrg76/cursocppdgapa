// vector2.cpp

#include <iostream>
// para la entrada/salida

#include <vector>
// para usar a la clase vector

int main()
{
	std::vector<std::string> nombres
	{
		"Fco.",
		"Javier",
	};

	for( auto x : nombres ){
		std::cout << x << std::endl;
	}

	std::string n;
	std::cout << "Introduzca su nombre: ";
	std::cin >> n;

	nombres.push_back( n );
	for( auto x : nombres ){ std::cout << x << std::endl; }
}
