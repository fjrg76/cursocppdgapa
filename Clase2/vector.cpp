// vector.cpp

#include <iostream>
// para la entrada/salida

#include <vector>
// para usar a la clase vector

int main()
{
	std::vector<int> v{ 2, 4, 6, 8, 10 };

	for(auto x : v ){
		std::cout << 2 * x  << std::endl;
	}

	v.push_back( 12 );
	for(auto x : v ){ std::cout << x << std::endl; }

}
