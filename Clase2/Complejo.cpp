// Complejo.cpp

#include <iostream>
#include <cmath>

class Complejo
{
private:
	double real{ 0.0 };
	double imag{ 0.0 };

public:
	void Poner( double _real, double _imag )
	{
		real = _real;
		imag = _imag;
	}

	void Poner_Real( double _real )
	{
		real = _real;
	}

	void Poner_Imag( double _imag )
	{
		imag = _imag;
	}

	void Imprimir()
	{
		std::cout << real << ", " << imag << std::endl;
	}

	double Obtener_Real()
	{
		return real;
	}

	double Obtener_Imag()
	{
		return imag;
	}

	Complejo Sumar( Complejo c )
	{
		Complejo r;
		
		r.real = real + c.real;
		r.imag = imag + c.imag;

		return r;
	}
};


int main(void)
{
	Complejo c;

	c.Imprimir();

	double resp;
	std::cout << "Por favor introduzca la parte real: ";
	std::cin >> resp;
	c.Poner_Real( resp );

	std::cout << "Por favor introduzca la parte imaginaria: ";
	std::cin >> resp;
	c.Poner_Imag( resp );

	c.Imprimir();

	Complejo x;
	x.Poner( 1.0, 1.0 );

	Complejo z = x.Sumar( c );
	z.Imprimir();

	return 0;
}
