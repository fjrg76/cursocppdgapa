
#include <iostream>


class Complejo
{
private:
	double real;
	double imaginario;

public:
	void Poner( double _real, double _im )
	{
		real = _real;
		imaginario = _im;
	}

	void Imprimir()
	{
		std::cout << real << " ," << imaginario << std::endl;
	}

	void Poner_Real( double _real )
	{
		real = _real;
	}

	Complejo Sumar( Complejo c )
	{
		Complejo r;
		r.real = real + c.real;
		r.imaginario = imaginario + c.imaginario;
		return r;
	}

	Complejo operator+( Complejo c )
	{
		Complejo r;
		r.real = real + c.real;
		r.imaginario = imaginario + c.imaginario;
		return r;
	}

	bool operator== ( Complejo c )
	{
		if( real == c.real and imaginario == c.imaginario ){
			return true;
		}
		else{
			return false;
		}
	}

	void operator++()
	{
		real = real + 1.0;
	}

	void operator++( int )
	{
		real = real + 1.0;
	}

};



int main(void)
{
	Complejo c;
	Complejo x;

	c.Poner( 3.14, 6.28 );
	x.Poner( 1, 1 );

	Complejo r = c + x;

	if( c == x ){
		std::cout << "Los números son iguales :) \n";
	}
	else{
		std::cout << "Los números NO son iguales :( \n";
	}

	r.Imprimir();

	++r;
	r.Imprimir();

	r++;
	r.Imprimir();
	
}
