// Complejo_con_const.cpp

#include <iostream>
#include <cmath>

class Complejo
{
private:
	double real{ 0.0 };
	double imag{ 0.0 };

public:
	
	// Constructor por defecto (no recibe argumentos)
	Complejo() : real{ 0.0 }, imag{ 0.0 }
	{
		// nada
	}

	// Constructor con un argumento
	Complejo( double _real ) : real{ _real }, imag{ 0.0 }
	{
		//nada
	}

	// Constructor con dos argumentos
	Complejo( double _real, double _imag ) : real{ _real }, imag{ _imag }
	{
		//nada
	}

	// Función miembro con un argumento con un valor por defecto
	void Poner( double _real, double _imag = 0.0 )
	{
		real = _real;
		imag = _imag;
	}

	void Poner_Real( double _real )
	{
		real = _real;
	}

	void Poner_Imag( double _imag )
	{
		imag = _imag;
	}

	void Imprimir()
	{
		std::cout << real << ", " << imag << std::endl;
	}

	double Obtener_Real()
	{
		return real;
	}

	double Obtener_Imag()
	{
		return imag;
	}

	Complejo Sumar( Complejo c )
	{
		Complejo r;
		
		r.real = real + c.real;
		r.imag = imag + c.imag;

		return r;
	}
};


int main(void)
{
	Complejo a;
	// usa al contructor por defecto (sin argumentos)
	
	Complejo b( 3.14 );
	// usa al constructor con un argumento
	
	Complejo c( 3.14, 6.28 );
	// usa al constructor con dos argumentos
	
	a.Imprimir();

	double resp{ 0.0 };
	std::cout << "Por favor introduzca la parte real: ";
	std::cin >> resp;
	b.Poner_Real( resp );

	std::cout << "Por favor introduzca la parte imaginaria: ";
	std::cin >> resp;
	b.Poner_Imag( resp );

	b.Imprimir();

	c.Poner( 1.0, 1.0 );

	Complejo z = c.Sumar( a );
	z.Imprimir();

	return 0;
}
