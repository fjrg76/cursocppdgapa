
// random1.cpp

#include <iostream>
#include <vector>

#include <random>
// para usar números aleatorios




int main()
{
	std::default_random_engine elGenerador;

	std::uniform_int_distribution<int> distUniformeInt( 10, 20 );
	// genera una distribución uniforme de números enteros en el rango
	// [10.0, 20.0]

	int cantidad;
	std::cout << "¿Cuántos números aleatorios desea generar? ";
	std::cin >> cantidad;

	std::cout << "Generando " << cantidad << " números aleatorios enteros...\n";
	for( int i = 0; i < cantidad; ++i ){
		std::cout << distUniformeInt( elGenerador ) << " ";
	}
	std::cout << std::endl;


	std::uniform_real_distribution<double> distUniformeDouble( 10, 20 );
	// genera una distribución uniforme de números reales en el rango
	// [10.0, 20.0)
	
	std::cout << "Generando " << cantidad << " números aleatorios reales...\n";
	for( int i = 0; i < cantidad; ++i ){
		std::cout << distUniformeDouble( elGenerador ) << " ";
	}
	std::cout << std::endl;

}
