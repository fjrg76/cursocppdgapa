
// random3.cpp
//
// Muestra la forma de mezclar una colección de números utilizando al motor de
// números aleatorios
//
// Está basado en random2.cpp

#include <iostream>
#include <vector>

#include <random>
// para usar números aleatorios

#include <chrono>
// para usar las funciones de reloj y cronómetro

#include <algorithm>
// para usar la función shuffle()



// Calcula un valor semilla en base al reloj del sistema
unsigned long CalculaSemilla()
{
	auto inicio = std::chrono::system_clock::now();

	// las siguientes tres instrucciones solamente "están haciendo tiempo":
	int cualquierValor;
	std::cout << "Introduzca cualquier valor entero: ";
	std::cin >> cualquierValor;

	auto dif = std::chrono::system_clock::now() - inicio;
	auto msec = std::chrono::duration_cast<std::chrono::milliseconds>( dif );
	return msec.count();
}

int main()
{
	auto horaDelSistema{ std::chrono::system_clock::now() };

	auto semilla = CalculaSemilla();
	std::default_random_engine elGenerador{ semilla };

	std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	std::cout << "Antes...\n";
	for( auto x : v ){
		std::cout << x << " ";
	}
	std::cout << std::endl;


	std::shuffle( 
			v.begin(), v.end(), // el rango
			elGenerador);       // la fuente de aleatoriedad



	std::cout << "Después...\n";
	for( auto x : v ){
		std::cout << x << " ";
	}
	std::cout << std::endl;



}
