
// random2.cpp
//
// Genera un valor semilla a partir del reloj del sistema

#include <iostream>
#include <vector>

#include <random>
// para usar números aleatorios

#include <chrono>
// para usar las funciones de reloj y cronómetro


// Calcula un valor semilla en base al reloj del sistema
unsigned long CalculaSemilla()
{
	auto inicio = std::chrono::system_clock::now();

	// las siguientes tres instrucciones solamente "están haciendo tiempo":
	int cualquierValor;
	std::cout << "Introduzca cualquier valor entero: ";
	std::cin >> cualquierValor;

	auto dif = std::chrono::system_clock::now() - inicio;
	auto msec = std::chrono::duration_cast<std::chrono::milliseconds>( dif );
	return msec.count();
}

int main()
{
	auto horaDelSistema{ std::chrono::system_clock::now() };

	auto semilla = CalculaSemilla();
	std::default_random_engine elGenerador{ semilla };

	std::uniform_int_distribution<int> distUniformeInt( 10, 20 );
	// genera una distribución uniforme de números enteros en el rango
	// [10.0, 20.0]

	int cantidad;
	std::cout << "¿Cuántos números aleatorios desea generar? ";
	std::cin >> cantidad;

	std::cout << "Generando " << cantidad << " números aleatorios enteros...\n";
	for( int i = 0; i < cantidad; ++i ){
		std::cout << distUniformeInt( elGenerador ) << " ";
	}
	std::cout << std::endl;


	std::uniform_real_distribution<double> distUniformeDouble( 10, 20 );
	// genera una distribución uniforme de números reales en el rango
	// [10.0, 20.0)
	
	std::cout << "Generando " << cantidad << " números aleatorios reales...\n";
	for( int i = 0; i < cantidad; ++i ){
		std::cout << distUniformeDouble( elGenerador ) << " ";
	}
	std::cout << std::endl;

}
